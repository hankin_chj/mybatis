package com.chj.mybatis.capt3;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import com.chj.mybatis.entity.EmailSexBean;
import com.chj.mybatis.entity.TUser;
import com.chj.mybatis.mapper.TUserMapper;
import com.chj.mybatis.mapper.TUserMapperExt;

public class MybatisDemo {
	
	private SqlSessionFactory sqlSessionFactory;

	@Before
	public void init() throws IOException {
		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		// 1.读取mybatis配置文件创SqlSessionFactory
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		inputStream.close();
	}
	
	/**
	 * Foreach用于in查询
	 */
	@Test
	public void testForeach4In() {
		// 2.获取sqlSession
		SqlSession sqlSession = sqlSessionFactory.openSession();
		// 3.获取对应mapper
		TUserMapperExt mapper = sqlSession.getMapper(TUserMapperExt.class);
		
		String[] names = new String[]{"lison","james"};
		List<TUser> users = mapper.selectForeach4In(names);
		System.out.println(users.size());
		for (TUser tUser : users) {
			System.out.println(tUser.toString());
		}
	}
	
	/**
	 * For each  用于批量插入
	 */
	@Test
	public void testForeach4Insert() {
		// 2.获取sqlSession
		SqlSession sqlSession = sqlSessionFactory.openSession();
		// 3.获取对应mapper
		TUserMapperExt mapper = sqlSession.getMapper(TUserMapperExt.class);
		
		TUser user1 = new TUser();
		user1.setUserName("king");
		user1.setRealName("李小京");
		user1.setEmail("li@qq.com");
		user1.setMobile("18754548787");
		user1.setNote("king's note");
		user1.setSex((byte)1);
		
		TUser user2 = new TUser();
		user2.setUserName("deer");
		user2.setRealName("陈大林");
		user2.setEmail("chen@qq.com");
		user2.setMobile("18723138787");
		user2.setNote("deer's note");
		user2.setSex((byte)1);
		
		int i = mapper.insertForeach4Batch(Arrays.asList(user1,user2));
		System.out.println(i);
		sqlSession.commit();
	}
	
	/**
	 * 	批量更新
	 */
	@Test
	public void testBatchExcutor() {
		// 2.获取sqlSession
//		SqlSession sqlSession = sqlSessionFactory.openSession(true);
		// 使用执行器 ExecutorType 设置默认提交false 则在手动执行提交后数据库数据才会更新
		SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
		// 3.获取对应mapper
		TUserMapperExt mapper = sqlSession.getMapper(TUserMapperExt.class);
		
		TUser user = new TUser();
		user.setUserName("zhangsan");
		user.setRealName("张三");
		user.setEmail("xxoo@163.com");
		user.setMobile("18695988747");
		user.setNote("zhangsan's note");
		user.setSex((byte) 1);
		user.setPositionId(1);
		System.out.println(mapper.insertSelective(user));
		
		TUser user1 = new TUser();
		user1.setId(3);
		user1.setUserName("lisi");
		user1.setRealName("李四");
		user1.setEmail("xxoo@163.com");
		user1.setMobile("18695988747");
		user1.setNote("lisi's note");
		user1.setSex((byte) 2);
		user1.setPositionId(1);
		System.out.println(mapper.updateIfOperate(user1));
		
		sqlSession.commit();
		System.out.println("----------------");

	}
	
	/**
	 * 	测试两种关联方式
	 */
	@Test
	public void testAssociation() {
		// 2.获取sqlSession
		SqlSession sqlSession = sqlSessionFactory.openSession();
		// 3.获取对应mapper
		TUserMapperExt mapper = sqlSession.getMapper(TUserMapperExt.class);
		// 4.执行查询语句并返回结果
		// ----------------------
		List<TUser> selectUserJobs1 = mapper.selectUserJobs1();
		for (TUser tUser : selectUserJobs1) {
			System.out.println(tUser);
		}
		System.out.println("----------------------------------------");
		// 一对多的关系
		List<TUser> selectUserJobs2 = mapper.selectUserJobs2();
		for (TUser tUser : selectUserJobs2) {
			System.out.println(tUser.toString());
		}
	}
	
	
	

}
