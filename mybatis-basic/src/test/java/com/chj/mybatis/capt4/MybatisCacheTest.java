package com.chj.mybatis.capt4;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import com.chj.mybatis.entity.TUser;
import com.chj.mybatis.mapper.TJobHistoryMapperExt;
import com.chj.mybatis.mapper.TUserMapperExt;

public class MybatisCacheTest {
	
	private SqlSessionFactory sqlSessionFactory;

	@Before
	public void init() throws IOException {
		
		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		// 1.读取mybatis配置文件创SqlSessionFactory
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		inputStream.close();
	}
	
	/**
	 *	 一级缓存 （也叫应用缓存）：一级缓存默认会启用，想要关闭一级缓存可以在select标签上配置flushCache=“true”；
	 *	一级缓存存在于 SqlSession 的生命周期中，在同一个 SqlSession 中查询时， MyBatis 会把执行的方法和参数通过算法生成缓存的键值，将键值和查询结果存入一个 Map对象中。
	 *	如果同一个 SqlSession 中执行的方法和参数完全一致，那么通过算法会生成相同的键值，当 Map 缓存对象中己经存在该键值时，则会返回缓存中的对象;
	 *	任何的 INSERT 、UPDATE 、 DELETE 操作都会清空一级缓存；
	 */
	@Test
	public void Test1LevelCache(){
		SqlSession session1 = sqlSessionFactory.openSession();
		TUserMapperExt userMapper1 = session1.getMapper(TUserMapperExt.class);
		String email = "qq.com";
		Byte sex = 1;
		List<TUser> list1 = userMapper1.selectByEmailAndSex2(email, sex);
		System.out.println(list1.size());
		
		//增删改操作会清空一级缓存和二级缓存
		TUser userInsert = new TUser();
		userInsert.setUserName("test1");
		userInsert.setRealName("realname1");
		userInsert.setEmail("myemail1");
//		userMapper1.insert1(userInsert);
		//
		List<TUser> list2 = userMapper1.selectByEmailAndSex2(email, sex);
		System.out.println(list2.toString());
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("email", email);
		map.put("sex", sex);
		List<TUser> list3 = userMapper1.selectByEmailAndSex1(map);
		System.out.println(list3.toString());
		
		session1.close();
		
		SqlSession session2 = sqlSessionFactory.openSession();
		TUserMapperExt userMapper2 = session2.getMapper(TUserMapperExt.class);
		List<TUser> list4 = userMapper2.selectByEmailAndSex2(email, sex);
		System.out.println(list4.toString());
		session1.close();
		
		
	}
	
	/**
	 *	 二级缓存存在于 SqlSessionFactory 的生命周期中，可以理解为跨sqlSession；缓存是以namespace为单位的，不同namespace下的操作互不影响。
	 * setting参数 cacheEnabled，这个参数是二级缓存的全局开关，默认值是 true，如果把这个参数设置为 false，即使有后面的二级缓存配置，也不会生效；
	 * 	要开启二级缓存,你需要在你的 SQL 映射文件中添加配置：<cache eviction="FIFO" flushInterval="60000" size="512" readOnly="true"/> 
	 */
	@Test
	public void Test2LevelCache(){
		SqlSession session1 = sqlSessionFactory.openSession();
		TUserMapperExt userMapper1 = session1.getMapper(TUserMapperExt.class);
		String email = "qq.com";
		Byte sex = 1;
		List<TUser> list1 = userMapper1.selectByEmailAndSex2(email, sex);
		System.out.println(list1.size());
		
//		TUser userInsert = new TUser();
//		userInsert.setUserName("test1");
//		userInsert.setRealName("realname1");
//		userInsert.setEmail("myemail1");
//		userMapper1.insert1(userInsert);
		// 默认先从二级缓存中查询，然后再到以及缓存
		List<TUser> list2 = userMapper1.selectByEmailAndSex2(email, sex);
		System.out.println(list2.toString());
		session1.close();
		
		SqlSession session2 = sqlSessionFactory.openSession();
		TUserMapperExt userMapper2 = session2.getMapper(TUserMapperExt.class);
		List<TUser> list3 = userMapper2.selectByEmailAndSex2(email, sex);
		System.out.println(list3.toString());
		session2.close();
		
		SqlSession session3 = sqlSessionFactory.openSession();
		TJobHistoryMapperExt userMapper3 = session3.getMapper(TJobHistoryMapperExt.class);
		List<TUser> list4 = userMapper3.selectByEmailAndSex2(email, sex);
		System.out.println(list4.toString());
		session3.close();

		
	}
	
	
	
	
}
