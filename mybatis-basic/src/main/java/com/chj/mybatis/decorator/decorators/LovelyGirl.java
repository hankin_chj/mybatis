package com.chj.mybatis.decorator.decorators;


import com.chj.mybatis.decorator.Girl;

public class LovelyGirl implements Girl {
	private Girl girl;

	public LovelyGirl(Girl girl) {
		super();
		this.girl = girl;
	}

	public void dance() {
		System.out.println("着装可爱……");
		girl.dance();
	}

}
