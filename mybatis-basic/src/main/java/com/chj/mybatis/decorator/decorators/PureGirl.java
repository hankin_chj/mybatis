package com.chj.mybatis.decorator.decorators;

import com.chj.mybatis.decorator.Girl;

public class PureGirl implements Girl {
	private Girl girl;

	public PureGirl(Girl girl) {
		super();
		this.girl = girl;
	}

	public void dance() {
		System.out.println("着装清纯……");
		girl.dance();

	}

}
