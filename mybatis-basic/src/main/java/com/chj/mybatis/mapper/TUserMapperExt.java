package com.chj.mybatis.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;

import com.chj.mybatis.entity.EmailSexBean;
import com.chj.mybatis.entity.TUser;
import com.chj.mybatis.entity.TUserExample;

public interface TUserMapperExt extends TUserMapper{
  
	// auto mapping query
	TUser selectByPrimaryKeyAutoMapper(Integer id);

	List<TUser> selectAll();

	List<TUser> selectTestResultMap();

	List<TUser> selectByEmailAndSex1(Map<String, Object> params);

	// must user annotation by query with param directly
	List<TUser> selectByEmailAndSex2(@Param("email")String email, @Param("sex")Byte sex);

	List<TUser> selectByEmailAndSex3(EmailSexBean esb);

	void insert1(TUser user1);

	void insert2(TUser user2);

    @Results(id="userInfo",value={
    		@Result(property="id",column="id",id = true),
    		@Result(property="userId",column="user_id"),
    		@Result(property="compName",column="comp_name"),
    		@Result(property="years",column="years"),
    		@Result(property="title",column="title")
    })
    @Select("select id, user_name, real_name, sex, mobile, email, note from t_user where id = #{userId}")
	List<TUser> selectByUserIdAnnotation(int userId);

    @ResultMap("userInfo")
    @Select("select id, user_name, real_name, sex, mobile, email, note from t_user")
	List<TUser> selectAllAnnotation();
    
    @Insert("insert into t_user (id, user_name, real_name, sex, mobile, email, note)"
    		+ "	values (#{id,jdbcType=INTEGER},#{userName,jdbcType=VARCHAR},#{realName,jdbcType=VARCHAR},"
    		+ "#{sex,jdbcType=TINYINT}, #{mobile,jdbcType=VARCHAR},"
    		+ "#{email,jdbcType=VARCHAR},#{note,jdbcType=VARCHAR})")
    @Options(useGeneratedKeys=true, keyProperty="id")
    int insertAnnotation(TUser record);

    //
	List<TUser> selectBySymbol(@Param("tableName")String tableName, @Param("inCol")String inCol, 
			@Param("orderStr")String orderStr, @Param("userName")String userName);

	List<TUser> selectIfOperate(@Param("email")String email, @Param("sex")Byte sex);
	
	List<TUser> selectIfandWhereOper(@Param("email")String email, @Param("sex")Byte sex);

	int updateIfOperate(TUser user);

	int updateIfAndSetOper(TUser user);

	int insertIfOperate(TUser user);

	List<TUser> selectForeach4In(String[] names);

	int insertForeach4Batch(List<TUser> asList);
	
    
    List<TUser> selectUserJobs1();
    
    List<TUser> selectUserJobs2();
    

    List<TUser> selectUserPosition1();
    
    List<TUser> selectUserPosition2();


	List<TUser> selectUserHealthReport();


	List<TUser> selectUserRole();


	List<TUser> selectByExample(TUserExample example);


    
}